from setuptools import setup

setup(
    name="auto-stopper",
    version="1.0",
    platforms="any",
    url="https://gitlab.com/desfrene/auto-stopper",
    license="GPL-3",
    packages=["auto_stopper"],
    author="Gabriel Desfrene",
    author_email="gabriel@desfrene.fr",
    description='Auto Stopper',
    entry_points={
        "console_scripts": [
            "auto-stopper=auto_stopper:main",
        ]
    },
    python_requires=">=3.9",
)
