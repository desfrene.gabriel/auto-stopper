import os
from pathlib import Path
from shutil import which
from subprocess import check_output, check_call
from time import sleep

POLL_SEC = 30
TIMEOUT_SEC = 60 * 5


def get_idle_time(xprintidle_cmd: str) -> float:
    output = check_output([xprintidle_cmd])
    return int(output) / 1000


def loop(xprintidle_cmd: str, sudo_cmd: str, shutdown_cmd: str):
    while get_idle_time(xprintidle_cmd) < TIMEOUT_SEC:
        sleep(POLL_SEC)

    check_call([sudo_cmd, shutdown_cmd, "+1"])


def main():
    xprintidle_cmd = which("xprintidle")
    sudo_cmd = which("sudo")
    shutdown_cmd = "/sbin/shutdown"

    if xprintidle_cmd is not None and sudo_cmd is not None and Path(shutdown_cmd).exists():
        if os.fork() == 0:
            loop(xprintidle_cmd, sudo_cmd, shutdown_cmd)
            exit(0)
        else:
            exit(0)
    else:
        print("Program (xprintidle, sudo, shutdown) not detected.")
        exit(1)
